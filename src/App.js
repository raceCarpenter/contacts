import React, { Component } from 'react';
import ContactList from './ContactList/ContactList';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ContactList />
      </div>
    );
  }
}

export default App;
