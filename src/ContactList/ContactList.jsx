import React, { Component } from 'react';
import Contact from '../Contact/Contact';
import Button from '@material-ui/core/Button';
import './ContactList.css';

class ContactList extends Component {
    constructor() {
        super();
        this.state = {
            contacts : [
                {   id: 1,
                    name: 'Joe',
                    phone: "4805259909",
                    email: 'joe@gmail.com',
                    address: '3255 N 75 Ave',
                },
                {   id: 2,
                    name: 'Wendy',
                    phone: "6025595855",
                    email: 'wendy@aol.com',
                    address: '4444 W 4th Drive',
                },
                {   id: 3,
                    name: 'Michael',
                    phone: "4809998888",
                    email: 'michael@gmail.com',
                    address: '667 E Michael St',
                },
                {   id: 4,
                    name: 'Phyllis',
                    phone: "4800975654",
                    email: 'phyllis@gmail.com',
                    address: '2222 S Phoenix Blvd',
                },
                {   id: 5,
                    name: 'Brian',
                    phone: "4802044444",
                    email: 'brian@gmail.com',
                    address: '565 N Fantasy Ave',
                },
            ],
            name: '',
            phone: '',
            email: '',
            address: '',
        }
        this.handleDelete = this.handleDelete.bind(this);
        this.addContact = this.addContact.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleDelete = index => {
        const { contacts } = this.state;
        contacts.splice(index, 1);
        this.setState({ contacts });
    };

    addContact = () => {
        const { contacts, name, phone, email, address} = this.state;
        this.setState({
          contacts: contacts.concat([{ name, phone, email, address}]),
        });
      }

    handleChange(e, i){
        const { contacts } = this.state;  
        const newArray = [...contacts.slice(0, i), 
            {...contacts[i], [e.target.name]: e.target.value},
             ...contacts.slice(i+1)];
        this.setState({
            contacts: newArray,
        })
    }

    render(){
        const list = this.state.contacts.map((c, i) => {
            return (
                <Contact 
                    key={i}
                    name={c.name} 
                    phone={c.phone} 
                    email={c.email} 
                    address={c.address}
                    onChange={e => this.handleChange(e, i)}
                    handleDelete={() => this.handleDelete(i)}
                />
            )
        })
        return (
            <div>
                Contacts 
                <div>
                    {list}
                </div>
                <Button 
                    variant="fab" 
                    onClick={() => this.addContact()}
                    color="primary" 
                    aria-label="Add"
                >
                    +
                </Button>
            </div>
        )
    }
}

export default ContactList;