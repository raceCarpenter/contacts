import React from 'react';
import './Contact.css'; 
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const Contact = ({ handleDelete, onChange, name, phone, email, address}) => (
<div className="container">
    <TextField
          id="name"
          label="Name"
          name="name"
          className="text-field"
          value={name}
          margin="normal"
          onChange={onChange}
        />
        <TextField
          id="phone"
          label="Phone"
          className="text-field"
          name="phone"
          value={phone}
          margin="normal"
          onChange={onChange}
        />
        <TextField
          id="email"
          label="Email"
          className="text-field"
          name="email"
          value={email}
          margin="normal"
          onChange={onChange}
        />
        <TextField
          id="address"
          label="Address"
          className="text-field"
          name="address"
          value={address}
          margin="normal"
          onChange={onChange}
        />
      <Button 
        style={{marginTop: '25px'}} 
        onClick={handleDelete} 
        color="secondary" 
        aria-label="Delete">
        X
      </Button>   
</div>
);

Contact.propTypes = {
    name: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.string,
    handleDelete: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};

Contact.defaultProps = {
    name: '',
    phone: '',
    email: '',
    address: '',
}


export default Contact;